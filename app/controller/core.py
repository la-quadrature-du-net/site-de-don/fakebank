#encoding: utf-8

from flask import render_template, request, redirect, session
import requests
import hashlib

import config
from app.controller.controller import Controller

class Core(Controller):
    def home(self):
        return render_template('core/home.html')

    def payment(self):
        """
        Processing fake payment request
        """
        # First of all, let's keep posted datas
        session['url_check'] = request.form.get('vads_url_check').replace('https', 'http')
        session['url_return'] = request.form.get('vads_url_return').replace('https', 'http')
        session['vads_trans_date'] = request.form.get('vads_trans_date')
        session['vads_trans_id'] = request.form.get('vads_trans_id')
        session['vads_order_id'] = request.form.get('vads_order_id')
        session['vads_action_mode'] = request.form.get('vads_action_mode')
        session['vads_ctx_mode'] = request.form.get('vads_ctx_mode')
        session['vads_language'] = request.form.get('vads_language')
        session['vads_page_action'] = request.form.get('vads_page_action')
        session['vads_site_id'] = request.form.get('vads_site_id')
        session['vads_presentation_date'] = request.form.get('vads_trans_date')
        session['vads_currency'] = request.form.get('vads_currency')
        session['vads_amount'] = request.form.get('vads_amount')
        session['vads_effective_amount'] = request.form.get('vads_amount')
        session['vads_effective_creation_date'] = request.form.get('vads_trans_date')
        session['vads_effective_currency'] = request.form.get('vads_currency')
        session['vads_payment_config'] = request.form.get('vads_payment_config')
        session['vads_cust_email'] = request.form.get('vads_cust_email')
        session['vads_identifier'] = request.form.get('vads_identifier')
        session['vads_sub_amount'] = request.form.get('vads_sub_amount')
        session['vads_sub_currency'] = request.form.get('vads_sub_currency')
        session['vads_sub_desc'] = request.form.get('vads_sub_desc')
        session['vads_sub_effect_date'] = request.form.get('vads_sub_effect_date')
        session['vads_effective_amount'] = request.form.get('vads_sub_amount')
        session['vads_effective_currency'] = request.form.get('vads_sub_currency')
        session['vads_amount'] = request.form.get('vads_sub_amount')
        session['vads_currency'] = request.form.get('vads_sub_currency')

        # Then let the user choose if payment is ok or not
        return render_template('core/payment.html')

    def valid(self):
        """
        Processing valid payment
        """
        # Preparing answer data
        params = {}
        try:
            params = {
                'vads_trans_status' : 'AUTHORISED',
                'vads_trans_date' : session['vads_trans_date'],
                'vads_trans_id' : int(session['vads_trans_id']),
                'vads_action_mode': session['vads_action_mode'],
                'vads_ctx_mode': session['vads_ctx_mode'],
                'vads_language': session['vads_language'],
                'vads_page_action': session['vads_page_action'],
                'vads_site_id': session['vads_site_id'],
                'vads_presentation_date': session['vads_trans_date'],
                'vads_order_id': session['vads_order_id'],

                'vads_expiry_month': '12',
                'vads_expiry_year': '2024',
                'vads_auth_mode': 'FULL',
                'vads_auth_number': '3fec2d',
                'vads_auth_result': '00',
                'vads_bank_code': '17807',
                'vads_bank_product': 'F',
                'vads_capture_delay': '0',
                'vads_card_brand': 'CB',
                'vads_card_country': 'FR',
                'vads_card_number': '497010XXXXXX0014',
                'vads_contract_used': '5201306',
                'vads_extra_result': '',
                'vads_hash': '74026bd3287e4241f30b1eac74ae6951a525989b57c320750dedbbbbafac4582',
                'vads_operation_type': 'DEBIT',
                'vads_payment_certificate': '3a179852ac1bb4b7c64722d1b20c4052e20cc596',
                'vads_payment_src': 'EC',
                'vads_pays_ip': 'FR',
                'vads_result': '00',
                'vads_sequence_number': '1',
                'vads_shop_name': 'Support La Quadrature du Net',
                'vads_threeds_cavv': 'Q2F2dkNhdnZDYXZ2Q2F2dkNhdnY=',
                'vads_threeds_cavvAlgorithm': '2',
                'vads_threeds_eci': '05',
                'vads_threeds_enrolled': 'Y',
                'vads_threeds_error_code': '',
                'vads_threeds_exit_status': '10',
                'vads_threeds_sign_valid': '1',
                'vads_threeds_status': 'Y',
                'vads_threeds_xid': 'VkZHZVViWXpscWRDQkRjdWNuakE=',
                'vads_trans_uuid': '6c673fec09d047bea48b8ac137c7d7e4',
                'vads_url_check_src': 'PAY',
                'vads_shop_url': '',
                'vads_validation_mode': '0',
                'vads_version': 'V2',
                'vads_warranty_result': 'YES',
            }
        except:
            print("error data basic")
        try:
            if session['vads_page_action']=='PAYMENT':
                params['vads_currency'] = session['vads_currency']
                params['vads_amount'] = session['vads_amount']
                params['vads_effective_amount'] = session['vads_amount']
                params['vads_effective_creation_date'] = session['vads_trans_date']
                params['vads_effective_currency'] = session['vads_currency']
                params['vads_payment_config'] = session['vads_payment_config']
        except:
            print("error data payment")
        try:
            if session['vads_page_action']=='REGISTER_SUBSCRIBE':
                params['vads_cust_email'] = session['vads_cust_email']
                params['vads_identifier'] = session['vads_identifier']
                params['vads_sub_amount'] = session['vads_sub_amount']
                params['vads_sub_currency'] = session['vads_sub_currency']
                params['vads_sub_desc'] = session['vads_sub_desc']
                params['vads_sub_effect_date'] = session['vads_sub_effect_date']
                params['vads_effective_amount'] = session['vads_sub_amount']
                params['vads_effective_currency'] = session['vads_sub_currency']
                params['vads_amount'] = session['vads_sub_amount']
                params['vads_currency'] = session['vads_sub_currency']
                params['vads_identifier_status'] = 'CREATED'
                params['vads_recurrence_status'] = 'CREATED'
                params['vads_subscription'] = 'SUBSCRIBED'
        except:
            print("error data subscribe")

        params['signature'] = compute_signature(params)

        print("="*42)
        print(params)
        print("="*42)
        print(session['url_check'])
        print(session['url_return'])

        # Calling the check page
        url_check = config.CHECK_URL
        if session['url_check']!='':
            url_check = session['url_check']
        if url_check!='':
            result = requests.post(url_check, data=params)
            print(result)
            with open('result.html', 'w') as f:
                f.write(result.text)

        # Then, we need to redirect to the return page
        if session['url_return']!='':
            return redirect(session['url_return'])

        # Else, let's display a single page
        return render_template('core/end.html')


def compute_signature(form):
    to_hash = '+'.join([str(form[f]) for f in sorted(form.keys()) if f.startswith('vads_')])
    to_hash += '+{}'.format(config.VADS_PAYMENT_CERTIFICATE)
    signature = hashlib.sha1()
    signature.update(to_hash.encode())
    return signature.hexdigest()

