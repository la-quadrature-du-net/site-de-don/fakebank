#encoding: utf-8

from app.controller.core import Core

routes = [
    ('/', Core.as_view('home')),
    ('/payment', Core.as_view('payment'), ['GET', 'POST']),
    ('/valid', Core.as_view('valid')),
#    ('/invalid', Core.as_view('invalid')),
]

apis = [
]
