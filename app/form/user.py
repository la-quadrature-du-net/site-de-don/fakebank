#encoding: utf-8

from flask_wtf import FlaskForm
from wtforms import StringField, FileField, SelectField, HiddenField, PasswordField
from wtforms.validators import DataRequired

class UserForm(FlaskForm):
    user_id = HiddenField('Id')
    login = StringField('Login', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password_confirm = PasswordField('Password confirmation')
    email = StringField('E-mail')

