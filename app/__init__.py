#encoding: utf-8

from flask_admin import Admin, expose, AdminIndexView
from flask_sqlalchemy import SQLAlchemy

from app.form.user import UserForm

class IndexView(AdminIndexView):
    @expose('/')
    def index(self):
        form = UserForm()
        return self.render('admin/index.html', form=form)

admin = Admin(index_view=IndexView())
db = SQLAlchemy()
