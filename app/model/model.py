#encoding: utf-8

from flask import redirect, request, url_for
from flask_admin.contrib.sqla import ModelView
from flask_login import current_user

from app import db

class View(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated and current_user.is_admin

    def inaccessible_callback(self, name, **kwargs):
        return redirect(url_for('admin.login', next=request.url))

class Model():
    def save(self):
        if self.id is None or self.id=='':
            db.session.add(self)
        db.session.commit()

    def delete(self):
        if self.id is not None:
            db.session.delete(self)
            db.session.commit()

