#encoding: utf-8

from app import admin, db
from app.model.model import Model, View

def get_user(user_id):
    return User.query.get(user_id)

class User(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(1000))
    password = db.Column(db.String(64))
    email = db.Column(db.String(1000))
    active = db.Column(db.Boolean())
    admin = db.Column(db.Boolean())

    def is_authenticated(self):
        return self.id is not None

    def is_active(self):
        return self.active

    def is_anonymous(self):
        return False

    def is_admin(self):
        return self.admin

    def get_id(self):
        return str(self.id)

class UserAdmin(View):
    column_exclude_list = ['password', ]

# User model will be automatically managed through flask-admin module
admin.add_view(UserAdmin(User, db.session))
