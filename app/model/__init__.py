#encoding: utf-8

import importlib
import os
import pkgutil

# Import automatically every model apart the base model 'model'
for module in pkgutil.iter_modules(path=(os.path.abspath(os.path.dirname(__file__)),)):
    name = module[1]
    capitalized_name = name[0].upper() + name[1:]
    if name!="model":
        getattr(importlib.import_module('app.model.%s' % name), capitalized_name)

