#encoding: utf-8

from flask import Flask, session, g
from flask_restful import Api
from flask_admin import Admin
from flask_babel import Babel
from flask_login import LoginManager

from app import admin, db

app = Flask(__name__, template_folder='app/view')
app.config.from_object('config')
if 'JINJA_ENV' in app.config:
    app.jinja_env.trim_blocks = app.config['JINJA_ENV']['TRIM_BLOCKS']
    app.jinja_env.lstrip_blocks = app.config['JINJA_ENV']['LSTRIP_BLOCKS']
admin.init_app(app)
api = Api(app)
babel = Babel(app)
login_manager = LoginManager(app)
db.init_app(app)

from app.routes import routes, apis

# Loading routes
for route in routes:
    if len(route)<3:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=route[2])
# Loading routes for api
for route in apis:
    api.add_resource(route[1], route[0])

# Manage locale
@babel.localeselector
def get_locale():
    #TODO: set locale even when session is cleared...
    if 'locale' not in session:
        session['locale'] = app.config['BABEL_DEFAULT_LOCALE']
    return session['locale']

# Manage user
from app.model.user import get_user
@login_manager.user_loader
def load_user(user_id):
    return get_user(user_id)

if __name__=='__main__':
    # Initialize Database
    from app import model
    with app.app_context():
        db.create_all()

    app.run(
        debug = app.config['DEBUG'],
        host = app.config['HOST'],
        port = app.config['PORT'],
    )

